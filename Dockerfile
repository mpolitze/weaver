# <!--
# SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
#
# SPDX-License-Identifier: CC-BY-SA-4.0 OR MIT
# -->

# # Building a Container to run Weaver

# Fist off we start with a build variable that allows setting the base image
# during build time. This is used to build weaver based on multiple
# distrbutions like `alpine` or `ubuntu`. If this is not set, alpine is used
# as a default. 
ARG BASE_IMAGE="docker.io/alpine/git:latest"

# Next we will use the previously defined base image to start building our
# container.
FROM ${BASE_IMAGE}

# Add the program into the container. We will put it into `/bin/` so it can be
# called from everywhere in the running container. Also make sure that the
# program is actually executable.
COPY ./lib/weave.awk /bin/weave

RUN chmod +x /bin/weave

# Finally define the entrypoint as `env` so a user can start off using the
# shell or any other command they desire.
ENTRYPOINT ["/usr/bin/env"]