<!--
SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# How To Use `weaver`

Assume you are writing a programm for which you want to provide an extensive
documentation to present that to your collegues (or your future self). Lets 
consider this beautiful example from one of our recent projects:

```ts
function makeArray(x:any){
    return [x].flat().filter((e:any)=>e);
}
function getLogo(entity:any){
    const logos = makeArray(entity.Logo);
    return logos.reduce((o, logo) => ({
        logo: logo.height * logo.width > o.size ? logo.text : o.logo,
        size: Math.max(logo.height * logo.width, o.size)
    }) , { size:-1, logo:undefined }).logo as string;
}
```

Of course one would expect some comments to be put in this file so we start out
by adding some `JSDoc`-style comments to document the usage of these functions.

```ts
/**
 * Make sure that the provided parameter is an array. Returns x if x is an
 * array, [x] otherwise.
 * @param x value to be wrapped as an array
 */
function makeArray(x:any){
    return [x].flat().filter((e:any)=>e);
}
/**
 * Get the best logo url from an entity. Returns logo url as string.
 * @param entity entity to extract the logo from
 */
function getLogo(entity:any){
    const logos = makeArray(entity.Logo);
    return logos.reduce((o, logo) => ({
        logo: logo.height * logo.width > o.size ? logo.text : o.logo,
        size: Math.max(logo.height * logo.width, o.size)
    }) , { size:-1, logo:undefined }).logo as string;
}
```

Ok. Much better. If people now want to use these functions they can see what
the functions are intending to do. However we are missing information about the 
design decisions that we took. So we add some more comments to the functions:

```ts
/**
 * Make sure that the provided parameter is an array. Returns x if x is an
 * array, [x] otherwise.
 * @param x value to be wrapped as an array
 */
function makeArray(x:any){
    // The flat function is a convenient way to get rid of nested arrays.
    // Also we make sure that the returned array does not contain any null
    // values using the filter function.
    return [x].flat().filter((e:any)=>e);
}
/**
 * Get the best logo url from an entity. Returns logo url as string.
 * @param entity entity to extract the logo from
 */
function getLogo(entity:any){
    // If the entity only contains a single logo it will not be an array, to
    // avoid a if else block here create a length 1 array if it is not already.
    const logos = makeArray(entity.Logo);
    // Since logos is definitely an array we can use the reduce function to
    // find the logo with the biggest size.
    return logos.reduce((o, logo) => ({
        // If the current elements' logo is bigger take that, if not keep the
        // on that was previously used.
        logo: logo.height * logo.width > o.size ? logo.text : o.logo,
        size: Math.max(logo.height * logo.width, o.size)
        // We start the reduce step with an undefined logo of size -1.
        // Every logo in the array should be bigger than that.
    }) , { size:-1, logo:undefined }).logo as string;
}
```

While this is certainly an exaggerated example, we can see now that most of the 
decisions that we made are becoming more clear and our train of thought can be 
reproduced by the reader of the comments. Probably it would help the reader to
have some more sophisticated formatting and structure in our code. We will use
Markdown for formatting and add some structure:

```ts
// # Logo Extraction Library
// This library provides functionalities to extract the right logos from data
// entities.
// ## Helpter Functions
// These functions are used as a utility during the logo extraction.
// ### Make Array Function
/**
 * Make sure that the provided parameter is an array. Returns `x` if `x` is an
 * array, `[x]` otherwise.
 * @param x the value to be wrapped as an array
 */
function makeArray(x:any){
    // The `flat` function is a convenient way to get rid of nested arrays.
    // Also we make sure that the returned array does not contain any `null`
    // values using the `filter` function.
    return [x].flat().filter((e:any)=>e);
}

// ## Core Functions
// The core functions for logo extraction
// ### Get Logo Function
/**
 * Get the best logo url from an entity. Returns logo url as string.
 * @param entity the entity to extract the logo from
 */
function getLogo(entity:any){
    // If the entity only contains a single logo it will not be an array, to
    // avoid a if else block here create a length `1` array if it is not
    // already.
    const logos = makeArray(entity.Logo);
    // Since logos is definitely an array we can use the `reduce` function to
    // find the logo with the biggest size.
    return logos.reduce((o, logo) => ({
        // If the current elements' logo is bigger take that, if not keep the
        // on that was previously used.
        logo: logo.height * logo.width > o.size ? logo.text : o.logo,
        size: Math.max(logo.height * logo.width, o.size)
        // We start the reduce step with an undefined logo of size `-1`.
        // Every logo in the array should be bigger than that.
    }) , { size:-1, logo:undefined }).logo as string;
}
```

Now we have our source code nice and readable as if it was an essay, that uses
headings and sections to make its content more human understandable. Making a 
habit from writing out the reasonning that lead to certain coding decision can 
greatly increase the quality and understandability of the written code. Also
re-phrasing in human readable language helps to reflect if the written source
code is doing what it is supposed to do.

While this is very readable while reading the source code, this is not very
suitable to supply as a documentation. So, if we now want to render this as a 
nice document is where `waever` comes into play. `weaver` will convert ("weave")
the source code file: extract the comments and put the actual lines of code
into code blocks. This is mainly intended to produce
[Markdown](https://en.wikipedia.org/wiki/Markdown) output that can then be
processed further into HTML pages or PDF documents using a variety of tools.

The application can be run directly from the command line using

```sh
weaver.awk code.ts
```

It will then print the Markdown code to the terminal. If you want to save the
output in a file you can use

```sh
weaver.awk code.ts > documentation.md
```

With the code from above your documentation file will then look like follows:

``````md
# Logo Extraction Library
This library provides functionalities to extract the right logos from data
entities.
## Helpter Functions
These functions are used as a utility during the logo extraction.
### Make Array Function

* Make sure that the provided parameter is an array. Returns `x` if `x` is an
* array, `[x]` otherwise.
* @param x the value to be wrapped as an array

```
 function makeArray(x:any){
```
The `flat` function is a convenient way to get rid of nested arrays.
Also we make sure that the returned array does not contain any `null`
values using the `filter` function.
```
    return [x].flat().filter((e:any)=>e);
}

```
## Core Functions
The core functions for logo extraction
### Get Logo Function

* Get the best logo url from an entity. Returns logo url as string.
* @param entity the entity to extract the logo from

```
function getLogo(entity:any){
```
If the entity only contains a single logo it will not be an array, to
avoid a if else block here create a length `1` array if it is not
already.
```
    const logos = makeArray(entity.Logo);
```
Since logos is definitely an array we can use the `reduce` function to
find the logo with the biggest size.
```
    return logos.reduce((o, logo) => ({
```
If the current elements' logo is bigger take that, if not keep the
on that was previously used.
```
        logo: logo.height * logo.width > o.size ? logo.text : o.logo,
        size: Math.max(logo.height * logo.width, o.size)
```
We start the reduce step with an undefined logo of size `-1`.
Every logo in the array should be bigger than that.
```
    }) , { size:-1, logo:undefined }).logo as string;
}
```
``````

For a more real world example, you can simply call `weaver` on itself using
```sh
weaver.awk weaver.awk > weaver.md
```
You can find more examples in the [`test` folder](./test).