<!--
SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Weaver

Weaver is a  is a
[Literate Programming](https://en.wikipedia.org/wiki/Literate_programming) tool
that produces documentation files from commented code files.

Weaver is written in [`awk`](https://de.wikipedia.org/wiki/Awk) to allow
maximum portability and only use tools available on small linux distributions
using BusyBox, like Alpine or limited Unix-type shells like "Git Bash" coming
with git for windows.

## Literate Programming

As seen ok [Wikipedia](https://en.wikipedia.org/wiki/Literate_programming):

> Literate programming is a programming paradigm introduced by Donald Knuth in
> which a computer program is given an explanation of its logic in a natural 
> language, such as English, interspersed with snippets of macros and 
> traditional source code, from which compilable source code can be generated.
> The approach is used in scientific computing and in data science routinely 
> for reproducible research and open access purposes. Literate programming 
> tools are used by millions of programmers today.

Literate programming differs from the usual way of dealing with source code and
documentation. Instead of producing side by side documents, literate programms
combine both in a single file that can be read like an essay that explains the
underlying programm code.

### How this works

See [HowTo.md](./HowTo.md) for a short tutorial.

### Why Create a New Implementataion There is X...

*There are quite a few literate prorgamming implementations why create another
one?* Most implementations start with a specialized "web" file format that is
then converted to the source code in a so called "tangling" process and to the
documentation in a so called "weaving" process.

```mermaid
graph LR
    A[Web Code] -->|tangle| B[Source Code]
    A -->|weave| C[Documentation Code]
    B -->|compile| D[Executable]
    C -->|process| E[Documentation]
```

This process has various advantages like being able to combine multiple
source code languages, capturing intermediate programm outputs or defintion of
macros (as for example allowed by
[org-babel](https://orgmode.org/worg/org-contrib/babel/intro.html)). While it
is quite possible to read through the documented file as a human witout
weaving, before running/compiling the the programm you need an additional step
for tangeling. To overcome this issue, we wanted a literate programming 
implemenatation that allows weaving from the source code directly and allows 
running/compiling the source code as it is.

```mermaid
graph LR
    B[Source Code]-->|weave| C[Documentation Code]
    B -->|compile| D[Executable]
    C -->|process| E[Documentation]
```

We were heavily inspired by the 
[`LiterateCS`](https://github.com/johtela/LiterateCS) project that provides a
similar, however much more sophisticated, functionalitiy for C# code. However,
we wanted an alternative that is more portable and works more independently of 
the programming language used.

## Development

Clone the repository, then make sure that `./lib/weave.awk` is executable:
```sh
chmod +x ./lib/weave.awk
```

The file `./lib/weave.awk` is actually the only source file, so open 
`./lib/weave.awk` in your favorite text editor and start coding.

While `awk` has a `gdb` powered debug mode using the `-D` flag I have not yet
used that. If you want to trace behaviours of the script you can set the 
`DEBUG` variable to `1` using the command line parameter `-v DEBUG=1`.

I like running something like
```sh
watch ./lib/weave.awk -v DEBUG=1 ./example.js
```
next to my text editor window to have something close to a "hot reload". For
convenience this repository also contains `watch.sh` a script that utilizes
[`pygmentize`](https://pygments.org/docs/cmdline/) to apply some highlighting
to the printed results.

You can find some sample files in the [`tests`](./tests) folder.

### Tests

There is a shell script `test.sh` that allows running a set of unit tests from
the [`tests`](./tests) folder and that creates a JUnit XML compliant report.

### Contributing

Contributions via merge requests are very welcome. When adding new features or
fixing a bug please also provide a test case that validates your changes.

Please make sure that your contribution passes the tests or give a reasonning
why changeing the behaviour is desirable in your merge request.

## License

The project was created in 2021 by Marius Politze <politze@itc.rwth-aachen.de>.

The repository aims to be [REUSE-compliant](https://reuse.software/) see
respective files for license information. In genral unless stated otherwise, 
Code is MIT Licensed, Documentation is CC-BY-SA-4.0 Licensed, Tests are CC0-1.0 
Licensed.
