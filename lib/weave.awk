#!/usr/bin/awk -f

# <!--
# SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
#
# SPDX-License-Identifier: CC-BY-SA-4.0 OR MIT
# -->

# # Weaving Markdown Commented File

# A `awk` script for weaving markdown commented files into markdown with code
# blocks.

# This way you can easily generate a documentation file e.g. from shell scripts
# and practice a way of code first
# [literate programming](https://en.wikipedia.org/wiki/Literate_programming).

# The script recognizes single line comments of different styles:
# * `#` for script stlye languages, like shell scripts or python,
# * `//` for C style languages, like C++ or Java 
# * `%` for TeX style languages, like LaTeX
# are documentation comments.
# Additionally multi line comments like
# * `/* ... */` for C style languages
# * `<!-- ... -->` for XML style languages
# are supported. All other lines are treated as code blocks.

# By default, the script assumes supports all styles at the same time, if this
# is causing issues (e.g. by handling `#include` directives in C as 
# documentation) you can use feature flags by setting the parameter `FFX` to 
# reduce the set of parsed comment styles.

# ## Usage

# You can run this script to weave `[filename]` using `awk` as an interpreter
# like follows:
# ```sh
# awk -f weave.awk [file]
# ```
# Additionally you may specify certain options to control the behaviour of
# the script using the pattern `-v [PARAMETER]=[VALUE]`:
# * `LANG` - language tag added to the beginning of code fences to allow syntax
#            highlighting in markdown. Default is `""`.
# * `CODE_FENCE` - Characters used to separate code from documentation blocks.
#                  Default is ```` "```" ````.
# * `FFX` - Feature flags to enable or disable certain behaviour of the script.
#           By default all features are enabled. See section 
#           [Feature Flags](#feature-flags) for usage details.
# * `DEBUG` - if set the script will print the current line mode at the
#             beginning of each line. Default is empty.

# > Fun fact: to test this out just call this script on itself using
# > ```sh
# > awk -f weave.awk -v LANG=awk weave.awk
# > ```

# ## Helper Functions

# Lets define a helper function that allows centally changeing the output
# behaviour of the script. To mimic the functionality of the print function we
# allow up to three parameters. The will be silently ignored if they are not
# present.
function out(o1, o2, o3)
{
  # In the documentation modes, remove leading white spaces, mostly to avoid
  # markdown thinking that they are indended code blocks.
  if(m~/d./ || m~/m./){
    sub(/^[ \t]*/, "", o1);
  }
  # If we are in DEBUG mode, print the current mode, then the rest of the line.
  if(DEBUG==1){
   print sprintf("%2s ", ml) sprintf("%2s ", m) o1 o2 o3;
  # Otherwise just print the line.
  }else{
    print o1 o2 o3;
  }
}

# ## Begin Block

# The block will be executed only once right before the
# rest of the script is executed.
BEGIN{

  # ### Code Fences

  # We then define the markdown to fence the documentation blocks from code
  # blocks. This typically is done using ```` ``` ````, however if the weaved
  # code heappens to include this as a literal it might break rendering  in
  # markdown so you might override this using additional backticks using the
  # commandline `` - v MD_FENCE="\`\`\`\`" `` (note as shown in this example,
  # you may have to additionally escape the `` ` `` char in the shell using
  # `` \` ´´).
  if(!CODE_FENCE) CODE_FENCE="```";
  FENCE_CODE_START=CODE_FENCE;
  FENCE_CODE_END=CODE_FENCE;

  # ### Feature Flags
  
  # Feature flags control the bahaviour of certain aspects of the script. They
  # can be enabled by setting the `FFX` variable on the commandl line using 
  # `-v FFX='...'`. If `FFX` ist not set all features are enabled.
  if(!FFX) FFX = "SHEBANG, STYLE_C, STYLE_SCRIPT, STYLE_XML, STYLE_TEX, MLSTRING_DQ, MLSTRING_BT, AWK_RS"

  # Then we use regular expressions to check wich of the feature flags are in
  # `FFX` string. Avoid using `\W` since that is not compatible with `mawk`
  # that is supplied in the ubuntu container image.

  # If check if the first line is a shebang `#!` and discard it.
  FF_SHEBANG=FFX~/(^|[- ,])SHEBANG([ ,]|$)/;
  # If set handle C style comments (`// ...` and `/* ... */`)
  FF_STYLE_C=FFX~/(^|[- ,])STYLE_C([ ,]|$)/;
  # If set handle script style comments ('# ...')
  FF_STYLE_SCRIPT=FFX~/(^|[- ,])STYLE_SCRIPT([ ,]|$)/;
  # If set handle XML style comments (`<!-- ... -->`)
  FF_STYLE_XML=FFX~/(^|[- ,])STYLE_XML([ ,]|$)/;
  # If set handle TeX style comments (`% ...`)
  FF_STYLE_TEX=FFX~/(^|[- ,])STYLE_TEX([ ,]|$)/;
  # If set handle multi line strings starting with double quotes (`"`)
  FF_MLSTRING_DQ=FFX~/(^|[- ,])MLSTRING_DQ([ ,]|$)/;
  # If set handle multi line string starting with backticks (`` ` ``)
  FF_MLSTRING_BT=FFX~/(^|[- ,])MLSTRING_BT([ ,]|$)/;
  # If set use `\r?\n` as row separator to handle windows line breaks.
  FF_AWK_RS=FFX~/(^|[- ,])AWK_RS([ ,]|$)/;

  # If `FFX` starts with a `-` the logic is inverted. That allows disabling 
  # some features without having to verbosely list all available feature flags.
  if(FFX~/^-/){
    FF_SHEBANG=!FF_SHEBANG;
    FF_STYLE_C=!FF_STYLE_C;
    FF_STYLE_SCRIPT=!FF_STYLE_SCRIPT;
    FF_STYLE_XML=!FF_STYLE_XML;
    FF_STYLE_TEX=!FF_STYLE_TEX;
    FF_MLSTRING_DQ=!FF_MLSTRING_DQ;
    FF_MLSTRING_BT=!FF_MLSTRING_BT;
    FF_AWK_RS=!FF_AWK_RS;
  }

  # ### `awk` Parsing Settings

  # Use built in variable `RS` "record separator" variable
  # (https://www.gnu.org/software/gawk/manual/html_node/User_002dmodified.html#index-RS-variable-2)
  # to make sure that parsing of files with LF and CRLF new lines are processed
  # equivaleently. If the user decided to redefine the variable.
  # > Note: using a regular expression here is a `gawk` extension, however this
  # > also works with the implementation available in Alpine (BusyBox v1.33.1).
  # > If this is causing trouble on your `awk` implementation you might be able
  # > to use `-v RS="\n "` (note the space afer `\n`) since non `gawk`
  # > implementations are supposed to only use the first character.
  # > If you still want to retain compatibility across LF and CRLF line endings
  # > consider using an external tool like 
  # > ```sh
  # > tr -d "\r" < file`
  # > ```
  # > to preprocess the file before passing it to this script.
  if(FF_AWK_RS && RS=="\n") RS="\r?\n";

  # ### Global Variables

  # Through out the program we will use a mode `m` that describes if the
  # current line is part of the documentation or the code.

  # `m` can have several values depending on the type of comment or line of
  # source code that is currently being processed:
  # * `m=="ds"`  - for single line *d*ocumentation *s*cript style (`#`)
  # * `m=="dc"`  - for single line *d*ocumentation *C* style (`//`)
  # * `m=="dt"`  - for single line *d*ocumentation *T*eX style (`%`)
  # * `m=="d_"`  - helper mode for empty single *d*ocumentation lines.
  # * `m=="mc"`  - for *m*ulti line *C* style comments (`/* ... */`)
  # * `m=="mx"`  - for *m*ulti line *X*ML style comments (`<!-- ... -->`)
  # * `m=="c"`   - for lines of source *c*ode
  # * `m=="sd"`  - for source code lines with *s*trings going over
  #              multiple lines using *d*ouble quotes (`"`)
  # * `m=="sb"`  - for source code lines with *s*trings going over
  #              multiple lines using *b*ackticks (`` ` ``)

  # Initially, we set the default mode to `0`.
  m="0";

  # For tracking changes over the mode across lines, we introduce a second
  # variable `ml` that always tracks the mode of the previous line. This is
  # initialized with a single line documentation mode to make sure a first
  # code line gets properly fenced.
  ml="d_";
}

# ## Loop Block

# The loop block is executed once for every line in the file. It goes through
# several regular expressions and excutes the respective blocks.

# Initially we reset the detected mode to the default mode `0`.
{
  m="0";
}

# ### Shebang Comment in Shell Scripts (`#!`)

# Ignore the first line if it is starting with shebang `#!` or a C style single
# line comment `//` that ends with `exit` in the documentation. They are likely
# intended to make the file runnable as script in linux shell. More explanation
# on this here:
# * https://unix.stackexchange.com/questions/162531/shebang-starting-with
# * https://en.wikipedia.org/wiki/Shebang_(Unix)
/^(#!.*|\/\/.*exit;?)$/{
  if(FF_SHEBANG && NR==1){
    next;
  }
}

# ### Handling Multi Line Comments

# #### C Style Multi Line Comments (`/* ... */`)

# ##### Start of C Style Multi Line Comments (`/*`)

# For C style languages `/*` start and `*/` ends a multi line comment. So if we
# encounter `/*` the processing mode should switch to multi line documentation
# mode `mc`.
/^[ \t]*\/\*/{
  # We will not switch if we were previously in multi line string mode,
  # comments there will be assumed to be part of the string literal.
  if(FF_STYLE_C && ml!~/s./){
    m="mc"
    # Then we remove the leading `/*` from the output. Sometimes people will use
    # multiple stars, so lets remove all of them `\*+`
    sub(/^ *\/\*+/, "");
  }
}

# ##### End of C Style Multi Line Comments (`*/`)

# If we heappen to be in multi line documentation mode `mc`, then `*/` ends
# the multi line mode and switches back to documentation mode `dc` for handling
# the last line.
# > Caveat: Source code that is placed in the same line
# > after `*/` is also handled as documentation.
/.*\*\//{
  if(ml=="mc" || m=="mc"){
    # In this case we remove the trailing `*/` from the output. Again we
    # tollerate users who wish to add more `*` chars like in `**/`.
    sub(/\*+\//, "");
    # Then switch to single line documentation mode.
    m="dc";
  }
}

# #### XML Style Multi Line Comments (`<!-- ... -->`)

# ##### Start of XML Style Multi Line Comments (`<!--`)

# For XML style languages `<!--` start and `-->` ends a multi line comment. So
# if we encounter `<!--` the processing mode should switch to multi line
# documentation mode `mx`.
/^[ \t]*(<!--.*)$/{
  # We will not switch if we were previously in multi line string mode,
  # comments there will be assumed to be part of the string literal.
  if(FF_STYLE_XML && ml!~/s./){
    m="mx"
    # Then we remove the leading `<!--` from the output.
    sub(/^ *<!--/, "");
  }
}

# ##### End of XML Style Multi Line Comments (`-->`)

# If we heappen to be in multi line documentation mode `mx`, then `-->` ends
# the multi line mode and switches back to documentation mode `d_` for handling
# the last line.
# > Caveat: Source code that is placed in the same line
# > after `-->` is also handled as documentation.
/^.*-->/{
  if(ml=="mx" || m=="mx"){
    # In this case we remove the trailing `-->` from the output and remove
    # leading spaces.
    sub(/ *-->/, "");
    # Then switch back to single line documentation mode.
    m="d_";
  }
}

# ### Single Line Comments (`#`, `%` or `//`)

# #### C Style Single Line Comments

# For lines starting with `//` we assume they are single line documentation in 
# C style.
/^[ \t]*\/\//{
  # To avoid duplicate procssing of multi and single line mode, only if still
  # in default mode `0` preprocess the line and set single line documentation
  # mode. If the previous line was a multi line string also do not process this
  # line.
  if(FF_STYLE_C && ml!~/s./ && ml!~/m./ && m=="0"){
    # Remove leading whitespaces and comment marks (`//`) from the line.
    sub(/^ *\/\//,"");
    m="dc";
  }
}

# #### Script Style Single Line Comments

# For lines starting with `#` we assume they are single line documentation in
# script style.
/^[ \t]*#/{
  # To avoid duplicate procssing of multi and single line mode, only if still
  # in default mode `0` preprocess the line and set single line documentation
  # mode. If the previous line was a multi line string also do not process this
  # line.

  if(FF_STYLE_SCRIPT && ml!~/s./ && ml!~/m./ && m=="0"){
    # Remove leading whitespaces and comment marks (`#`) from the line.
    sub(/^ *#/,"");
    m="ds";
  }
}

# #### TeX Style Single Line Comments

# For lines starting with `%` we assume they are single line documentation in 
# TeX style.
/^[ \t]*%/{
  # To avoid duplicate procssing of multi and single line mode, only if still
  # in default mode `0` preprocess the line and set single line documentation
  # mode. If the previous line was a multi line string also do not process this
  # line.
  if(FF_STYLE_TEX && ml!~/s./ && ml!~/m./ && m=="0"){
    # Remove leading whitespaces and comment marks (`%`) from the line.
    sub(/^ *%/,"");
    m="dt";
  }
}

# ### Empty Lines

# For empty lines, we simply continue with the mode of the last line.
/^[ \t]*$/{
  if(ml!~/m./ && ml!=/s./ && m=="0"){
    # An alternate implementation would be to use documentation mode for empty
    # lines:
    # > ```
    # > m="d_";
    # > ```
    m=ml;
  }
}

# ### Multi Line Stings (Non-Paired `"` or `` ` ``)

# Finally, detect lines that do not have matching quotes (odd number of quote
# chars in a line). This pattern should also work for languages like python
# that use `"""` to start a multi line string literal but may produce some
# false positives in case of syntax errors.
#
# For JavaScript style languages we additionally allow `` ` `` used by template
# strings to start a multi line string.
#
# We are explicitely distinguishing between the two strings in the mode `m`
# (using `sd` and `sb` as mode respectively) to make sure that while in the one
# multi line string mode we will not accidentally switch to the other.
#
# > Caveat: This may lead to some odd behaviour if both modes are combined in
# > one document and two multi line string blocks end/start in the same line.
#
# #### Double Quoted Strings (`"`)
#
# First lets start with double quoted (`"`) multi line strings:
# * They start with some non-quote characters: `^[^"]*`
# * Then There are pairs of matching quotes, possibly followed by more non-qote
#   characters: `("(...)*"[^"]*)*`
# * Between the pair of quotes there may be non-quote characters or escaped
#   quotes (`\\"`): `\\"|[^"]`
# * After the last pair of quotes there is a single quote followed by only non
#   quote characters and a line ending: `"[^"]*$`
/^[^"]*("(\\"|[^"])*"[^"]*)*"[^"]*$/{
  # If the current line is still in undefined mode, the multi line string
  # started. Switch mode to `sd` for "*s*tring *d*ouble quoted" for the next
  # line.
  if(FF_MLSTRING_DQ && m=="0"){
    m="sd";
  }
  # If we were in mode `sd`, the multi line string ended. We will switch to `c`
  # for handling the last line.
  if(ml=="sd"){
    m="c";
  }
}

# #### Backticked Strings (`` ` ``)
#
# Next up we recognize backticked (`` ` ``) multi line strings:
# * They start with some one non-backtick characters: `` ^[^`]* ``
# * Then there are pais of matching backticks, possibly followed by more non
#   backtick characters: `` (`(...)+`[^`]*)* ``
# * Between the pairs of backticks there have to be non-backtick characters or
#   escaped backticks (`` \\` ``): `` \\`|[^`] ``
# > Caveat: This is to work around issues where a string `` "```" `` would
# > trigger this mode. Probably better would be to ignore backticks in quotes
# > alltogether.
# * After the last pair of backticks, there is a single backtick followed by
#   only non backtick characters and the line ending: `` `[^`]*$ ``
/^[^`]*(`(\\`|[^`])+`[^`]*)*`[^`]*$/{
  # If the current line is still in unfedined mode, the multi line string
  # started. Switch mode to `sb` for "*s*tring *b*acktick" for the next line.
  if(FF_MLSTRING_DQ && m=="0"){
    m="sb";
  }
  # If we were in mode `sb`, the multi line string ended. We will switch back 
  # `c` for handling the last line.
  if(ml=="sb"){
    m="c";
  }
}

# ### Printing Lines

# Finally the currently processed line went through various gates and the mode
# should be defined based on the lines contents.
{
  # If none of the above conditions were met, the current line is still in
  # default mode. Depending on the last line we decide what to do next:
  if(m=="0"){
    # Either the last line was in single line documentation mode `d.`, then
    # the current line is just a normal line of code ...
    if(ml~/d./){
      m="c";
    }
    # ... or we were in some multi line (string oder documentation) mode and
    # continue that.
    else{
      m=ml;
    }
  }
  # Now we definitely have a mode set. If we are switching from any of the 
  # documentation modes (`d.` or `m.`) to one of the code modes (`c` or `s.`)
  # or vice versa, we print the respective start or end code fence first.
  if((ml~/d./ || ml~/m./) && (m=="c" || m~/s./)){
    out(FENCE_CODE_START, LANG);
  }
  if((ml=="c"|| ml~/s./) && (m~/d./ || m~/m./)){
    out(FENCE_CODE_END);
  }
  # Then we print the actual line.
  out($0);
  # Finally save the current mode for the next line.
  ml = m;
}

# ## End Block

# The end block is executed once after all lines have been processed.

# If we are still in a code block or a multi line string, end it.
END{
  if(m=="c" || m~/s./){
    out(FENCE_CODE_END);
  }
}
