
<!--
SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>

SPDX-License-Identifier: CC-BY-SA-4.0 OR MIT
-->

# Run Tests for `weave.awk`

## Helper Functions

The script should create a test report that can be parsed by GitLabs
[test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html) feature.
To achieve this, the report has to comply to JUnit XML format.

### About JUnit XML

[JUnit XML](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format) 
format was originally designed for reporting test results in the Java
programming language ecosystem. I became a de-facto standard for reporting
test cases of other programming languages and most testing frameworks have
converters or output formatters for this  format. You can find a JUnit XML
Schema
[here](https://github.com/windyroad/JUnit-Schema/blob/master/JUnit.xsd).
GitLab offers to parse JUnit XML files to create graphical report e.g. in 
pipelines or in merge requests.

## Test Execution

First make sure that the script is executable.
```
chmod +x ./lib/weave.awk

```
We will need some temporary files to store the results of the test cases.
Lets create them now:
```
TMP_DIFF=$(mktemp)
TMP_ERR=$(mktemp)
TMP_ACTUAL=$(mktemp)

```
Go through all files (`-type f`) startting with `tc-` (for "*t*est *c*ase")
in the directory `./test`.
```
for TESTCASE in $(find "./test/" -mindepth 1 -maxdepth 1 -type d); do
    echo "Running test case: " $TESTCASE
```
Make sure that we always start "clean" so truncate temporary files from
previous test cases.
```
    truncate -s 0 "$TMP_DIFF"
    truncate -s 0 "$TMP_ERR"
    truncate -s 0 "$TMP_ACTUAL"

    TC_IN=$(find "$TESTCASE" -maxdepth 1 -type f -name "in.*")
    TC_EXPECTED=$(find "$TESTCASE" -maxdepth 1 -type f -name "expected.*")
    TC_ARGS=$(find "$TESTCASE" -maxdepth 1 -type f -name "args.*")

    if [ ! -f "$TC_IN" ]; then
        echo "================================"
        echo "Test case '$TESTCASE' failed:"
        echo "No input file found."
        echo "================================"
        continue
    fi

    if [ ! -f "$TC_EXPECTED" ]; then
        echo "================================"
        echo "Test case '$TESTCASE' failed:"
        echo "No expected file found."
        echo "================================"
        continue
    fi

    if [ -f "$TC_ARGS" ]; then
```
Run `weave.awk` for the file, but also pass the command line
arguments from the args file. Save the result in a temporary file for
easier evauliation.
```
        ARGS="$(cat "$TC_ARGS" | tr -d '\r?\n') $TC_IN"
        echo "$ARGS" | xargs ./lib/weave.awk 2> "$TMP_ERR" > "$TMP_ACTUAL"
    else
```
Run `weave.awk` for the file, save the result in a temporary file for
easier evauliation.
```
        ./lib/weave.awk "$TC_IN" 2> "$TMP_ERR" > "$TMP_ACTUAL"
    fi

```
If `awk` did not exit cleanly, the script likely did not run at all.
Report the error that `awk` gave and report the test case as failed.
```
    if [ "$?" != 0 ] ; then
        echo "================================"
        echo "Test case '$TESTCASE' failed:"
        cat "$TMP_ERR"
        echo "================================"
        continue
    fi

```
Files ending with `.ex.md` contain the *ex*pected output of the script.
This is compared to the actual output in the temporary file created
before.
```
    diff -du "$TC_EXPECTED" "$TMP_ACTUAL" 2> "$TMP_ERR" > "$TMP_DIFF"

```
If diff did not exit cleanly, then the files are different and thus the
test case failed. Report the failure.
```
    if [ "$?" != 0 ] ; then
        echo "================================"
        echo "Test case '$TESTCASE' failed:"
        cat "$TMP_DIFF"
        cat "$TMP_ERR"
        echo "================================"
    fi
done

```
Finally make sure we are not leaving behind any stray files. In case the
test script errored.
```
rm "$TMP_DIFF"
rm "$TMP_ERR"
rm "$TMP_ACTUAL"
```
