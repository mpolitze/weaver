# HTML Example Document
```html
<!DOCTYPE html>
```
This document is a simple example of an HTML Document with comments.
```html
<html>
```
## Body Tag
```html
<body>
```

Within the body there are the
main elements of this page.

```html
    <h1 id="a">My First Heading</h1>
    <p>My first paragraph.</p>
    <script type="text/javascript">
```
### A Script Block
A funny thing in HTML ist, that it can contain JavaScript code.
JavaScript allows multi line strings using backticks:
```html
    var s = `
    // A Comment here is part of the code!
    Das ist ein \`text\`.
    `;

```

Of course also multi line comments can work. and should
be rendered accordingly.


```html
    document.getElementById('a').text = s;

```
## A loop

sometimes we just loop around.
```html
    for(var i=0; i<100; i++){ console.log(i); }
    </script>
</body>
</html>
```
