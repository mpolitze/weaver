#!/bin/sh

# # A Wondeful Shell Test

# Lets print a simple string
echo "Hello World"

# Check someting interesting
if [ ! -d "." ]; then
    echo "Wow. The current directory is a directory"
fi

# This is about multi line strings:
echo "
    Multi line strings in Shell mode
    # This is not a comment!
"