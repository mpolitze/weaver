// # Logo Extraction Library
// This library provides functionalities to extract the right logos from data
// entities.
// ## Helpter Functions
// These functions are used as a utility during the logo extraction.
// ### Make Array Function
/**
 * Make sure that the provided parameter is an array. Returns `x` if `x` is an
 * array, `[x]` otherwise.
 * @param x the value to be wrapped as an array
 */
 function makeArray(x:any){
    // The `flat` function is a convenient way to get rid of nested arrays.
    // Also we make sure that the returned array does not contain any `null`
    // values using the `filter` function.
    return [x].flat().filter((e:any)=>e);
}

// ## Core Functions
// The core functions for logo extraction
// ### Get Logo Function
/**
 * Get the best logo url from an entity. Returns logo url as string.
 * @param entity the entity to extract the logo from
 */
function getLogo(entity:any){
    // If the entity only contains a single logo it will not be an array, to
    // avoid a if else block here create a length `1` array if it is not
    // already.
    const logos = makeArray(entity.Logo);
    // Since logos is definitely an array we can use the `reduce` function to
    // find the logo with the biggest size.
    return logos.reduce((o, logo) => ({
        // If the current elements' logo is bigger take that, if not keep the
        // on that was previously used.
        logo: logo.height * logo.width > o.size ? logo.text : o.logo,
        size: Math.max(logo.height * logo.width, o.size)
        // We start the reduce step with an undefined logo of size `-1`.
        // Every logo in the array should be bigger than that.
    }) , { size:-1, logo:undefined }).logo as string;
}