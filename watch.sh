# <!--
# SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
#
# SPDX-License-Identifier: CC-BY-SA-4.0 OR MIT
# -->

# # Convenience Watch Script with Syntax Highlighting

# This script is used for development purposes. It regularly calls the program
# in debug mode, then separates the debug information from the generated
# output, applys syntax highlighting to the output and puts everything back
# together.

# ## Global Variables

# ### Script File

# The path to the script being tested.
SCRIPT_FILE="./lib/weave.awk"

# ## Main Program

# We will run an infinite loop until this script is either forcefully cancelled
# using `[Ctrl]-[c]` or by gracefully pressing `[q]`.
while true; do
    # ### Applying Syntax Highlighting

    # Run the script in debug mode and capture the output.
    out=$($SCRIPT_FILE -v DEBUG=1 $1)
    # The first 6 chars are the debug output added to the printed lines.
    # We use `cut` to get them and capture the output.
    debug=$(echo "$out" | cut -c -6)
    # Everything after and including the 7th character is the actual output.
    file=$(echo "$out" | cut -c 7-)
    # Now pass the actual output to `pygmentize` for highlighting.
    highlighted=$(echo "$file" | pygmentize -l md)
    # Finally put everything back together. We use `-d'\0'` to avoid an
    # additional tab being added between the segments.
    pasted=$(paste -d'\0' <(echo "$debug") <(echo "$highlighted"))

    # ### Printing
    # Get the number of lines that currently fit on the terminal and strip the
    # as many lines as we can fit on the screen (`-2` to compensate for 
    # trailing new lines).
    nolines=$(tput lines)
    lines=$(echo "$pasted" | head -n $(expr $nolines - 2))
    # Clear what we previously had on the terminal and print as
    # We are doing this together in one buffer to avoid flickering on the
    # screen. (Idea from 
    # https://unix.stackexchange.com/questions/81167/prevent-text-screen-blinking-when-doing-clear#comment675237_126153)
    tmp=$(clear; echo "$lines"; echo "")
    echo "$tmp"

    # ### Graceful Exit
    # Listen to inputs from the user and write them to the variable `$key`
    read -r -s -n1 -t2 key
    # If the user pressed `[q]` break the loop and therefore end the script.
    if [ "$key" = "q" ]; then
        break
    fi
done